<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package trees
 */

?>

</section><!-- #content -->

<footer id="colophon" class="site-footer-wrapper">
    <div class="container">
        <div class="quote-motto-wrapper">
            <div>
                <blockquote class="quote-motto">
                    <p class="quote-motto__text">
                        <?php echo get_theme_mod('quote_setting'); ?>
                    </p>
                </blockquote>
                <p class="quote-motto__author">
                    <?php echo get_theme_mod('quote_author_setting'); ?>
                </p>
            </div>
        </div>
    </div>
    <div class="social-networks">
        <div class="container">
            <h4 class="social-networks__title">
                <?php echo get_theme_mod('social_networks_title_setting'); ?>
            </h4>
            <ul class="social-networks__list">
                <li class="social-networks__item">
                    <a class="social-networks__link" href="<?php echo get_theme_mod('social_networks_link_1_setting'); ?>">
                        <i class="<?php echo get_theme_mod('social_networks_icon_1_setting'); ?>" aria-hidden="true"></i>
                    </a>
                </li>
                <li class="social-networks__item">
                    <a class="social-networks__link" href="<?php echo get_theme_mod('social_networks_link_2_setting'); ?>">
                        <i class="<?php echo get_theme_mod('social_networks_icon_2_setting'); ?>" aria-hidden="true"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="copyright-wrapper">
        <div class="container">
            <span>
                <?php echo  get_theme_mod('left_text_copyright_setting'); ?>
            </span>
            &copy;
            <span>
                2020 -
                <?php echo date('Y'); ?>
                <?php echo get_theme_mod('right_text_copyright_setting'); ?>
            </span>
        </div>
    </div>
    <div class="principle-belief">
        <div class="container">
            <?php echo get_theme_mod('principle-belief_text_setting'); ?>
        </div>
    </div>
</footer>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>