<?php
/**
 * Template name: Contact
 */
get_header();
?>
    <section class="main container">
        <?php
        $my_lang = pll_current_language(); // определяем текущий язык

        if ( $my_lang == 'ru' ) {
            echo do_shortcode('[contact-form-7 id="7" title="Contact form Ru"]');
        }
        else {
            echo do_shortcode('[contact-form-7 id="125" title="Contact form Ukranian"]');
        }
        ?>
    </section>
<?php
get_footer();
