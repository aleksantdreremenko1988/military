<?php
/*
 * Template Name: News
 */
get_header();
?>

<?php
    $args = [];

    query_posts($args);

    while (have_posts()) : the_post();

        get_template_part('template-parts/content', 'news');

    endwhile;
    wp_reset_query()
?>

<?php
get_footer();
