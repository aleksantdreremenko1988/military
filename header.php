<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package trees
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
    <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'trees' ); ?></a>

    <header id="masthead" class="site-header-wrapper">
        <div class="site-header container">
            <div class="mobile-top-header">
                <div class="site-branding">
                    <?php
                    the_custom_logo();
                    if ( is_front_page() && is_home() ) :
                        ?>
                        <h1 class="site-title">
                            <a class="site-title__link" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                                <span class="site-title__first-part">
                                    <span>
                                        <?php echo get_theme_mod('logo_text_first_letter_first_part_setting') ?>
                                    </span>
                                    <?php echo get_theme_mod('logo_text_first_part_setting') ?>
                                </span>
                                <span class="site-title__second-part">
                                    <span>
                                        <?php echo get_theme_mod('logo_text_first_letter_second_part_setting') ?>
                                    </span>
                                    <?php echo get_theme_mod('logo_text_second_part_setting') ?>
                                </span>
                            </a>
                        </h1>
                    <?php
                    else :
                        ?>
                        <h1 class="site-title">
                            <a class="site-title__link" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                                <span class="site-title__first-part">
                                    <span>
                                        <?php echo get_theme_mod('logo_text_first_letter_first_part_setting') ?>
                                    </span>
                                    <?php echo get_theme_mod('logo_text_first_part_setting') ?>
                                </span>
                                <span class="site-title__second-part">
                                    <span>
                                        <?php echo get_theme_mod('logo_text_first_letter_second_part_setting') ?>
                                    </span>
                                    <?php echo get_theme_mod('logo_text_second_part_setting') ?>
                                </span>
                            </a>
                        </h1>
                    <?php endif; ?>
                </div><!-- .site-branding -->
                <div class="hamburger-menu-wrapper">
                    <div class="hamburger-menu">
                        <div class="hamburger-menu__line-1"></div>
                        <div class="hamburger-menu__line-2"></div>
                        <div class="hamburger-menu__line-3"></div>
                    </div>
                </div>
            </div>
            <div class="site-header__navigation-language">
                <nav id="site-navigation" class="header-navigation">
                    <?php
                    wp_nav_menu( array(
                        'theme_location' => 'menu-1',
                        'menu_id'        => 'primary-menu',
                        'container' => false
                    ) );
                    ?>

                </nav><!-- #site-navigation -->
                <div class="site-header__switch-language">
                    <?php pll_the_languages(
                        array(
                            'dropdown' => 0, // простой список, 1 — выпадающий список (по умолчанию: 0)
                            'show_flags'=>1, //показывать картинки флагов (по умолчанию: 0)
                            'show_names'=>0, //вывод названий языков (по умолчанию: 1)
                            'hide_current'=> 1
                        )
                    );
                    ?>
                </div>
            </div>
        </div>
    </header><!-- #masthead -->

    <section id="content" class="container site-content">