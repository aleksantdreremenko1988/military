<?php
/**
 * Template name: Digest
 *
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Phantom_Lite
 */
get_header();
?>
    <section class="main container">
        <?php
        $args = [
            'post_type' => 'digest',
        ];

        query_posts($args);

        while (have_posts()) : the_post();

            get_template_part('template-parts/content', 'digest');

        endwhile; // End of the loop.
        wp_reset_query()
        ?>
    </section>
<?php
get_footer();