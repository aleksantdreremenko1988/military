<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package militarythemes
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">

	<?php
	// You can start editing here -- including this comment!
	if ( have_comments() ) :
		?>
		<h2 class="comments-title">
			<?php
			$militarythemes_comment_count = get_comments_number();
			if ( '1' === $militarythemes_comment_count ) {
                $my_lang = pll_current_language();
                if ( $my_lang == 'ru' ) {
                    printf(
                    /* translators: 1: title. */
                        esc_html__( 'Один коментарий о &ldquo;%1$s&rdquo;', 'militarythemes' ),
                        '<span>' . wp_kses_post( get_the_title() ) . '</span>'
                    );
                } else {
                    printf(
                    /* translators: 1: title. */
                        esc_html__( 'Один коментар щодо &ldquo;%1$s&rdquo;', 'militarythemes' ),
                        '<span>' . wp_kses_post( get_the_title() ) . '</span>'
                    );
                 }
			} else {
                $my_lang = pll_current_language();
                if ( $my_lang == 'ru' ) {
                    printf(
                    /* translators: 1: comment count number, 2: title. */
                        esc_html( _nx( '%1$ коментрииев о &ldquo;%2$s&rdquo;', '%1$s коментриия о &ldquo;%2$s&rdquo;', $militarythemes_comment_count, 'comments title', 'militarythemes' ) ),
                        number_format_i18n( $militarythemes_comment_count ), // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
                        '<span>' . wp_kses_post( get_the_title() ) . '</span>'
                    );
                } else {
                    printf(
                    /* translators: 1: comment count number, 2: title. */
                        esc_html( _nx( '%1$s коментрів про &ldquo;%2$s&rdquo;', '%1$s коментаря про &ldquo;%2$s&rdquo;', $militarythemes_comment_count, 'comments title', 'militarythemes' ) ),
                        number_format_i18n( $militarythemes_comment_count ), // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
                        '<span>' . wp_kses_post( get_the_title() ) . '</span>'
                    );
                }

			}
			?>
		</h2><!-- .comments-title -->

		<?php the_comments_navigation(); ?>

		<ol class="comment-list">
			<?php
			wp_list_comments(
				array(
					'style'      => 'ol',
					'short_ping' => true,
				)
			);
			?>
		</ol><!-- .comment-list -->

		<?php
		the_comments_navigation();

		// Якщо коментарі закриті, а є коментарі, залишимо невеличку примітку, чи не так?
		if ( ! comments_open() ) :
			?>
			<p class="no-comments">
                <?php
                $my_lang = pll_current_language();
                if ( $my_lang == 'ru' ) {
                    esc_html_e( 'Комментарии закрыты', 'militarythemes' );
                } else {
                    esc_html_e( 'Коментарі закриті', 'militarythemes' );
                }
                ?>

            </p>
			<?php
		endif;

	endif; // Check for have_comments().

	comment_form();
	?>

</div><!-- #comments -->
