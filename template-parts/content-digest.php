<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header">
        <?php the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' ); ?>
    </header>

    <?php




    $fields = CFS()->get( 'loop_for_digest_news' );
    if( ! empty($fields) ):
        foreach ( $fields as $field ) {
            ?>
                <h2>
                    <?php echo $field['text_for_news_in_digest']; ?>
                </h2>
           
            <a href="<?php echo wp_get_attachment_image_url( $field['upload'], 'full' );?>">
                <?php echo wp_get_attachment_image( $field['image_for_news_in_digest'], 'medium'); ?>
            </a>


            <?php




        };
    endif;?>









    <div class="entry-content">
        <?php the_excerpt(); ?>
    </div>

    <a href="<?php the_permalink(); ?>">
        Read More
    </a>
</article>