<article id="post-<?php the_ID(); ?>" <?php post_class($class = 'one-news'); ?>>
    <a class="one-news__link" href="<?php echo the_permalink(); ?>">
        <h2 class="one-news__title">
            <?php the_title(); ?>
        </h2>
        <div class="one-news__image-entry-content">
            <div class="one-news__image">
                <?php echo get_the_post_thumbnail(); ?>
            </div>
            <div class="one-news__entry-content">
                <?php the_excerpt(); ?>
            </div>
        </div>

    </a>
</article>