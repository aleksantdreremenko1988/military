<?php
/**
 * militarythemes Theme Customizer
 *
 * @package militarythemes
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function militarythemes_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial(
			'blogname',
			array(
				'selector'        => '.site-title a',
				'render_callback' => 'militarythemes_customize_partial_blogname',
			)
		);
		$wp_customize->selective_refresh->add_partial(
			'blogdescription',
			array(
				'selector'        => '.site-description',
				'render_callback' => 'militarythemes_customize_partial_blogdescription',
			)
		);
	}
}
add_action( 'customize_register', 'militarythemes_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function militarythemes_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function militarythemes_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function militarythemes_customize_preview_js() {
	wp_enqueue_script( 'militarythemes-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'militarythemes_customize_preview_js' );


/* sections, settings, and controls for customize */

function wptuts_customize_register( $wp_customize ) {

    /* sections, settings, and controls for "Header" */

    $wp_customize->add_setting( 'logo_text_first_letter_first_part_setting' , array(
        'default'   => __('', 'wptuts'),
        'transport' => 'refresh',
    ));

    $wp_customize->add_control(
        'logo_text_first_letter_first_part_setting',
        array(
            'label'    => __( 'First letter in text for first part logo', 'wptuts' ),
            'section'  => 'header_section',
            'settings' => 'logo_text_first_letter_first_part_setting',
            'type'     => 'text',
        )
    );

    $wp_customize->add_setting( 'logo_text_first_part_setting' , array(
        'default'   => __('', 'wptuts'),
        'transport' => 'refresh',
    ));

    $wp_customize->add_control(
        'logo_text_first_part_setting',
        array(
            'label'    => __( 'Text for first part logo', 'wptuts' ),
            'section'  => 'header_section',
            'settings' => 'logo_text_first_part_setting',
            'type'     => 'text',
        )
    );

    $wp_customize->add_setting( 'logo_text_first_letter_second_part_setting' , array(
        'default'   => __('', 'wptuts'),
        'transport' => 'refresh',
    ));

    $wp_customize->add_control(
        'logo_text_first_letter_second_part_setting',
        array(
            'label'    => __( 'First letter in text for second part logo', 'wptuts' ),
            'section'  => 'header_section',
            'settings' => 'logo_text_first_letter_second_part_setting',
            'type'     => 'text',
        )
    );

    $wp_customize->add_setting( 'logo_text_second_part_setting' , array(
        'default'   => __('', 'wptuts'),
        'transport' => 'refresh',
    ));

    $wp_customize->add_control(
        'logo_text_second_part_setting',
        array(
            'label'    => __( 'Text for second part logo', 'wptuts' ),
            'section'  => 'header_section',
            'settings' => 'logo_text_second_part_setting',
            'type'     => 'text',
        )
    );

    $wp_customize->add_section( 'header_section' , array(
        'title'      => __( 'Header', 'wptuts' ),
        'priority'   => 1,
    ));

    /* sections, settings, and controls for "contacts" */

    $wp_customize->add_setting( 'title_field_name_in_contact_page_setting' , array(
        'default'   => __('', 'wptuts'),
        'transport' => 'refresh',
    ));

    $wp_customize->add_control(
        'title_field_name_in_contact_page_setting',
        array(
            'label'    => __( 'Title for field "Name" in "Contacts page"', 'wptuts' ),
            'section'  => 'contacts_section',
            'settings' => 'title_field_name_in_contact_page_setting',
            'type'     => 'text',
        )
    );

    $wp_customize->add_section( 'contacts_section' , array(
        'title'      => __( 'Contacts', 'wptuts' ),
        'priority'   => 100,
    ));





















    /* sections, settings, and controls for "footer" */

    $wp_customize->add_setting( 'quote_setting' , array(
        'default'   => __('', 'wptuts'),
        'transport' => 'refresh',
    ));

    $wp_customize->add_control(
        'quote_setting',
        array(
            'label'    => __( 'Text for quote', 'wptuts' ),
            'section'  => 'footer_section',
            'settings' => 'quote_setting',
            'type'     => 'text',
        )
    );

    $wp_customize->add_setting( 'quote_author_setting' , array(
        'default'   => __('', 'wptuts'),
        'transport' => 'refresh',
    ));

    $wp_customize->add_control(
        'quote_author_setting',
        array(
            'label'    => __( 'Author for quote', 'wptuts' ),
            'section'  => 'footer_section',
            'settings' => 'quote_author_setting',
            'type'     => 'text',
        )
    );

    $wp_customize->add_setting( 'social_networks_title_setting' , array(
        'default'   => __('', 'wptuts'),
        'transport' => 'refresh',
    ));

    $wp_customize->add_control(
        'social_networks_title_setting',
        array(
            'label'    => __( 'Title for social networks', 'wptuts' ),
            'section'  => 'footer_section',
            'settings' => 'social_networks_title_setting',
            'type'     => 'text',
        )
    );

    $wp_customize->add_setting( 'social_networks_link_1_setting' , array(
        'default'   => __('', 'wptuts'),
        'transport' => 'refresh',
    ));

    $wp_customize->add_control(
        'social_networks_link_1_setting',
        array(
            'label'    => __( 'Link for social networks №1', 'wptuts' ),
            'section'  => 'footer_section',
            'settings' => 'social_networks_link_1_setting',
            'type'     => 'text',
        )
    );

    $wp_customize->add_setting( 'social_networks_icon_1_setting' , array(
        'default'   => __('', 'wptuts'),
        'transport' => 'refresh',
    ));

    $wp_customize->add_control(
        'social_networks_icon_1_setting',
        array(
            'label'    => __( 'Icon for social networks №1', 'wptuts' ),
            'section'  => 'footer_section',
            'settings' => 'social_networks_icon_1_setting',
            'type'     => 'text',
        )
    );

    $wp_customize->add_setting( 'social_networks_link_2_setting' , array(
        'default'   => __('', 'wptuts'),
        'transport' => 'refresh',
    ));

    $wp_customize->add_control(
        'social_networks_link_2_setting',
        array(
            'label'    => __( 'Link for social networks №2', 'wptuts' ),
            'section'  => 'footer_section',
            'settings' => 'social_networks_link_2_setting',
            'type'     => 'text',
        )
    );

    $wp_customize->add_setting( 'social_networks_icon_2_setting' , array(
        'default'   => __('', 'wptuts'),
        'transport' => 'refresh',
    ));

    $wp_customize->add_control(
        'social_networks_icon_2_setting',
        array(
            'label'    => __( 'Icon for social networks №2', 'wptuts' ),
            'section'  => 'footer_section',
            'settings' => 'social_networks_icon_2_setting',
            'type'     => 'text',
        )
    );

    $wp_customize->add_setting( 'left_text_copyright_setting' , array(
        'default'   => __('', 'wptuts'),
        'transport' => 'refresh',
    ));

    $wp_customize->add_control(
        'left_text_copyright_setting',
        array(
            'label'    => __( 'Text for left part copyright', 'wptuts' ),
            'section'  => 'footer_section',
            'settings' => 'left_text_copyright_setting',
            'type'     => 'text',
        )
    );

    $wp_customize->add_setting( 'right_text_copyright_setting' , array(
        'default'   => __('', 'wptuts'),
        'transport' => 'refresh',
    ));

    $wp_customize->add_control(
        'right_text_copyright_setting',
        array(
            'label'    => __( 'Text for right part copyright', 'wptuts' ),
            'section'  => 'footer_section',
            'settings' => 'right_text_copyright_setting',
            'type'     => 'text',
        )
    );

    $wp_customize->add_setting( 'principle-belief_text_setting' , array(
        'default'   => __('', 'wptuts'),
        'transport' => 'refresh',
    ));

    $wp_customize->add_control(
        'principle-belief_text_setting',
        array(
            'label'    => __( 'Text for principle and belief', 'wptuts' ),
            'section'  => 'footer_section',
            'settings' => 'principle-belief_text_setting',
            'type'     => 'text',
        )
    );

    $wp_customize->add_setting( 'right_text_copyright_setting' , array(
        'default'   => __('', 'wptuts'),
        'transport' => 'refresh',
    ));

    $wp_customize->add_control(
        'right_text_copyright_setting',
        array(
            'label'    => __( 'Text for right part copyright', 'wptuts' ),
            'section'  => 'footer_section',
            'settings' => 'right_text_copyright_setting',
            'type'     => 'text',
        )
    );

    $wp_customize->add_section( 'footer_section' , array(
        'title'      => __( 'Footer', 'wptuts' ),
        'priority'   => 100,
    ));

}
add_action( 'customize_register', 'wptuts_customize_register' );
